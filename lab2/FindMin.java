public class FindMin{
	public static void main(String[] args){
		if(args.length==3){
			int value1=Integer.parseInt(args[0]);
			int value2=Integer.parseInt(args[1]);
			int value3=Integer.parseInt(args[2]);
			int min;
			
			System.out.println("a = "+value1+" , "+"b = "+value2+" , "+"c = "+value3);
			boolean condition=value1<value2;
			min=condition?value1:value2;
			condition=min<value3;
			min=condition?min:value3;
			System.out.println("Minimum = "+min);
		}
	}
}